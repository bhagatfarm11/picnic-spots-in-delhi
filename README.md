Life in a bustling city like Delhi can get very dull, exhausting and dreary. The steady weight and worry of work, 
the horrible traffic, the long drives and plan-less ends of the week regularly spent in shopping centers or 
seeing family can influence life to appear as though you are simply making a cursory effort. 
All things considered, it's a great opportunity to wake up and accomplish something other 
than what's expected. Investing energy in nature can do marvels to support your inclination and enhance 
your viewpoint. What's more, Delhi has no lack of incredible outdoorsy spots to unwind in. Peruse our rundown 
Picnic Spots in Delhi you can appreciate. Know more visit http://www.drbhagatfarm.com/gallery.html
